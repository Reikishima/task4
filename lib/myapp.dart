import 'package:get/get.dart';
import 'package:flutter/material.dart';

 void main() =>
  runApp(const GetMaterialApp(home: myApp()));
// ignore: camel_case_types
class myApp extends StatelessWidget {
  const myApp({super.key});
static const appTitle = "Task 2";

  @override 
  Widget build (BuildContext context){
    return const MaterialApp(title: appTitle, home: MyHomePage(title : appTitle),);
}
}
int _selectedIndex = 0;
class MyHomePage extends StatefulWidget{
  const MyHomePage ({super.key, required this.title});
  @override
  State<MyHomePage> createState() => _MyHomePage();
  final String title;
}
class _MyHomePage extends State<MyHomePage>{
   static final List<Widget> _widgetOption = <Widget>[
   const Text('data'),
    const Text('data'),
    const Text('data')
  ];
  
 void _onItemTapped(int index){
      setState((){
        _selectedIndex = index;
      });
      
  }
  @override 
  Widget build (BuildContext context)
  {
    
    return Scaffold(appBar: AppBar(title: const Text('Task 2')),body: Center(child: _widgetOption.elementAt(_selectedIndex),),
     bottomNavigationBar: BottomNavigationBar(items: const <BottomNavigationBarItem>[BottomNavigationBarItem(icon: Icon(Icons.home),label: 'Home'),
    BottomNavigationBarItem(icon: Icon(Icons.picture_in_picture_alt_rounded),label: 'Picture'),
    BottomNavigationBarItem(icon: Icon(Icons.color_lens),label: 'Custom Paint')
    ],
    currentIndex: _selectedIndex,
    selectedItemColor: Colors.amber[800],
    onTap: _onItemTapped,
    ),
    drawer: Drawer(
      // add list view di dalam drawer
      //through the option if isn't enough for vertical
      //space to fit everything.
      child: ListView(
        //remove any padding in listview
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(decoration: BoxDecoration(color: Color.fromARGB(255, 8, 210, 15)),
          child: Text('Menu'),
          ),
          ListTile(
            title: const Text('Item 1'),onTap: () {
            },
          ),
          ListTile(
            title: const Text('Item 2'),onTap: () {
              Navigator.pop(context);
            },
          )
        ],
      ),
    ), 
    );
  }
}