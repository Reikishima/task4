
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:task4/myapp.dart';

class LoginController extends GetxController{

 

final loginFormKey=GlobalKey<FormState>();
late TextEditingController emailController,passwordController;
final email = '';
final password= '';
var isPasswordHidden=true.obs;

  @override 
  void onInit(){
    super.onInit();
    emailController = TextEditingController();
    passwordController = TextEditingController();
  }



  @override 
  void onClose(){
    emailController.dispose();
    passwordController.dispose();
    super.onClose();
    
  }
  String? validator (String value){
    if(value.isEmpty){
      return 'Please this Field must be filled';
    }
    return null;
  }

  validateEmail(value){
    if(!GetUtils.isEmail(value)){
      return 'bukti email valid';
    }
    return null;
  }
 validatePassword(value){
    if(value.isEmpty){
      return 'Password harus 4- 6 karakter';
    }else if(value.lenght > 7){
      return 'too much';
    }
    return null;
  }

  void checkLogin(){
      if (emailController.text != email && passwordController.text !=password ){
        Get.off(const myApp());
      }else{
        Get.snackbar('Login','Invalid Email or Password');
      }
  }
  
  }

