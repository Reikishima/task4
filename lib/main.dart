import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task4/login_controller.dart';
import 'package:task4/myapp.dart';

void main() => runApp(  GetMaterialApp(home: Home()));


class Home extends StatelessWidget {

final controller = Get.put(LoginController());
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        body:Wrap(children: <Widget>[
            Center(child: Form(
              key:controller.loginFormKey,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              child: Column(
                children: [
                Image.asset('Images/download.jpg',width: 100,height: 250,alignment: Alignment.center,),
                const SizedBox(
                  height: 20,
                ),
                const Text("Welcome in Wings Bird",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18,color: Color.fromARGB(255, 9, 51, 74))
                ),const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: InputDecoration(hintText: 'Masukkan Email dengan Benar!',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10)
                    ),
                    labelText: "Email",
                    prefixIcon: const Icon(Icons.email),
                   
                  ),
                  keyboardType: TextInputType.emailAddress,
                  controller: controller.emailController,
                  onSaved: (value){
                    controller.email == value!;
                  },
                  validator: (value){
                    if(value !=null){
                    return 'isi email';
                  }
                  return controller.validateEmail(value!);
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                Obx(() => 
                TextFormField(
                  decoration: InputDecoration(hintText: 'Password Harus 4-6 Karakter',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    labelText: "Password",
                    prefixIcon: const Icon(Icons.lock),
                     suffix:  InkWell(child: const Icon(Icons.visibility,color:  Color.fromARGB(255, 34, 104, 202),size: 20,),
                     onTap: () {
                       controller.isPasswordHidden.value =! controller.isPasswordHidden.value;
                     },
                     ), 
                    isDense: true
                  ),
                  obscureText: controller.isPasswordHidden.value,
                  controller: controller.passwordController,
                  onSaved: (value){
                    controller.password == value!;
                  },
                  validator: (value){
                    if(value !=null){
                   return 'isi password' ;
                    }
                    return controller.validatePassword(value!);
                  },
                  
                ),
                ),
                const SizedBox(
                  height: 10,
                ),
                ConstrainedBox(constraints: BoxConstraints.tightFor(width: context.width,),
                child: ElevatedButton(
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                    backgroundColor: MaterialStateProperty.all(Colors.lightBlue),padding: MaterialStateProperty.all(const EdgeInsets.all(14))
                  ),
                  child: const Text('Login',style: TextStyle(fontSize: 14,color : Colors.white,),
                  ),
                  onPressed: () {
                    controller.checkLogin();
                    
                  },
                  ))
                ]
                ),
        ))],
      ),backgroundColor: Colors.grey,
      ),
      );
  }
  
}


class MyHomePage extends StatefulWidget {
 

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".


  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _isHidePassword = true;

  void _tooglePasswordVisibility() {
    setState(() {
      _isHidePassword = !_isHidePassword;
    });
  }
  
  @override
  Widget build(Object context) {
    return Scaffold(
      appBar: AppBar()
      ,body: Container(
        child: _buildPasswordField(),
      ),
    );
  
    // TODO: implement build
    throw UnimplementedError();
    
  }

  Widget _buildPasswordField(){
    return TextFormField(

    );
  }

}
